import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  constructor() { }

  x: boolean;

  ngOnInit() {

    this.x = (window.screen.width)>441;
    console.log(this.x);

    setTimeout(()=>{
      document.getElementById('bob').classList.add("bobs");
    },1500)
  }

}
